package com.anderson.salesreport.business.registro.importacao;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Component;

import com.anderson.salesreport.business.enums.IdentificadorRegistroEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Component
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Getter
@CsvRecord(separator = ";")
public class Venda implements RegistroImportacao{
	
	@Transient
	@DataField(pos = 1, required = true, trim = true)
	private final String identificadorRegistro = IdentificadorRegistroEnum.VENDA.getCodigo();
	
	@Id
	@DataField(pos = 2, required = true, trim = true)
    private Integer saleId;
	
	@DataField(pos = 3, required = true, trim = true)
    private Integer idItem;
	
	@DataField(pos = 4, required = true, trim = true)
	private Double qdtItem;
	
	@DataField(pos = 5, required = true, trim = true)
	private Double precoItem;
	
	@DataField(pos = 6, required = true, trim = true)
    private String salesmanName;
	
	private Double saleAmount;
	
	public Double getTotalVenda() {
		this.saleAmount = this.qdtItem * this.precoItem;
		return this.saleAmount;
	}

}
