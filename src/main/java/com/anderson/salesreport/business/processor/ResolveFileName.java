package com.anderson.salesreport.business.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Component
public class ResolveFileName implements Processor{
	
	private static final String DAT_PROCESSADO = ".proc";

	@Override
	public void process(Exchange exchange) throws Exception {
		String filename = exchange.getIn().getHeader(Exchange.FILE_NAME,String.class);
		exchange.getIn().setHeader(Exchange.FILE_NAME, StringUtils.join(filename, DAT_PROCESSADO));
		
		log.info(StringUtils.joinWith(" ","Nome do arquivo que será gerado:",
				exchange.getIn().getHeader(Exchange.FILE_NAME,String.class)));
		
	}

}
